# A small case study of cross origin requests

There are two *ExpressJS* apps in this repository.
The first one is called `cookie-app`, which generates and manipulates cookies. The second one is aptly called `consumer`, which simply puts the `cookie-app` to the test.

Aside from express, both of the two systems depend on raw JS and HTML to see the browser enforced HTTP rules to work.

# Running both of them

Make sure you have the following installed:

- `node`
- `npm`

## Configuraion

First thing's first, add the following to your host file:

```
127.0.0.1 demo_cookie_app
127.0.0.1 demo_cookie_creation_app
```

## Installing dependencies

For both folders, execute:

    $ npm i

## Running

First, you run the `cookie-app` by navigating to the directory
and executing:

    $ node .

Then, on another terminal window or session,
navigate to the `consumer` directory and execute the same command as shown above:

    $ node .

# End result

With both apps running, open `http://demo_cookie_app:7871/`.
You should now be able to modify another domain's (`demo_cookie_creation_app`) cookie data without having to leave the current domain (`demo_cookie_app`)

If you want to generate cookies just for testing purposes, you can always open this url:

    http://demo_cookie_creation_app:7872/make-cookies/{quantity}/{size}

**Just make sure you replace `{quantity}` and `{size}` with the values that matter to you ❤️**