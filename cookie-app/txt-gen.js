
const options = [
    // alpha
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
    'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
    's', 't', 'u', 'v',
    // num
    '1', '2', '3', '4', '5', 
    '6', '7', '8', '9', '0',
    '.', ',', '-', '_', '!'
]

function randomChar(){
    return options[Math.floor(Math.random() * options.length)]
}

function randomSeq(size=0){
    let output = ''
    for (let i = 0; i < size; i++) {
        output += randomChar()
    }
    return output;
}

module.exports = {
    randomChar,
    randomSeq
}