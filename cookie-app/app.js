const express = require('express')
const bodyparser = require('body-parser')
const cookieParser = require('cookie-parser')
const assert = require('assert')
const utils = require('./utils')
const app = express()


app.use(bodyparser.json())
app.use(bodyparser.urlencoded({extended: true}))
app.use(cookieParser())


app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", req.get('origin'));
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Methods", "GET, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "content-type");
    next()
})

app.use((req, res, next)=>{
    if(req.method.toLowerCase() == "options"){
        return res.status(200).send()
    }
    else{
        return next()
    }
})


app.post('/cookie', (req, res)=>{
    var cookieName = req.body.name;
    var cookieValue = req.body.value;
    assert(typeof cookieName == 'string', 'cookie name is empty')
    assert(cookieName != undefined, 'cookie value is empty')

    const expiration = utils.makeExpiration()
    
    res.cookie(cookieName, String(cookieValue), {
        httpOnly : true,
        expires: expiration
    })
    return res.json({
        message : 'cookie added',
        body : {
            name : cookieName,
            value : cookieValue
        }
    })
})

app.delete('/cookie/:name', (req, res)=>{
    var cookieName = req.params.name;
    assert(typeof cookieName == 'string', 'cookie name is empty')
    res.clearCookie(cookieName)

    return res.json({
        message : 'cookie deleted',
        body : {
            name : cookieName,
        }
    })
})



app.get('/cookie', (req, res)=>{
    return res.json({
        message : 'list of cookies',
        body : {
            ...req.cookies
        }
    })
})

app.get('/cookie/:name', (req, res)=>{
    var cookieName = req.params.name;
    assert(typeof cookieName == 'string', 'cookie name is empty')
    
    let rgexp = new RegExp(cookieName
        .replace(/\W/g, ' ')
        .replace(/ +/g, '.*'), 'i')

    let output = {}

    Object.keys(req.cookies || {})
        .filter(cookie => rgexp.test(cookie))
        .forEach(cookie => output[cookie] = req.cookies[cookie])
    
    return res.json({
        message : 'filtered list of cookies',
        body : {
            ...output
        }
    })
})

const generator = require('./txt-gen')

app.get('/make-cookies/:number/:size', (req, res, next)=>{
    let size = Number(req.params.size), number = Number(req.params.number);

    assert(size, 'not a valid number')
    assert(number, 'not a valid number')

    const expiration = utils.makeExpiration()
    size = Math.floor(size)

    let output = {}

    for (let i = 0; i < number; i++) {
        let valA = generator.randomSeq(size);
        let valB = generator.randomSeq(size);
        output[valA] = valB;
        res.cookie(valA, valB, {
            httpOnly : true,
            expires : expiration
        })
    }

    return res.json({
        message : 'cookies generated :)',
        body : {
            ... output
        }
    });
})

module.exports = app