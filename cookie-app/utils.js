

const defaultValue = Number(process.env.MAX_AGE_COOKIE) || 86400000

function makeExpiration(suggestion=null){
    return new Date(new Date().getTime() + Number(process.env.MAX_AGE_COOKIE))
}

module.exports = {
    makeExpiration
}