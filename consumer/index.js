const dotenv = require('dotenv')
dotenv.config()

const app = require('./app')

app.listen(process.env.PORT, ()=>{
    console.log(`Listening on http://${process.env.HOST}:${process.env.PORT}/`);
});