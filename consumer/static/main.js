
const registerButton = document.getElementById("register_cookies")
const deleteButton = document.getElementById("delete_cookies")
const getButton = document.getElementById("get_info")
const listButton = document.getElementById("list_cookies")
const content = document.getElementById("content")
const inputKey = document.getElementById("input_key")
const inputValue = document.getElementById("input_value")
const url = "http://demo_cookie_creation_app:7872/";

var api = (function(){
    const myApi = axios.create({
        baseURL: url,
        timeout: 10000,
        withCredentials: true,
        // crossDomain: true,
        headers: {
            'Content-Type': 'application/json',
        }
    });
    
    function listAll(){
        return myApi.get("/cookie")
    }

    function filter(name){
        if(!(typeof name == 'string')) {
            throw new TypeError("Name is not a string")
        }
        return myApi.get("/cookie/" + name);
    }

    function save(dookie){
        if (!dookie || !dookie.name || !dookie.value){
            throw new TypeError("Object is incomplete")
        }
        return myApi.post("/cookie", dookie)
    }

    function del(name){
        if(!(typeof name == 'string')) {
            throw new TypeError("Name is not a string")
        }
        return myApi.delete("/cookie/" + name);
    }

    return {
        listAll,
        filter,
        save,
        del
    }
})()

function getObjectFromFields(){
    return {
        name : inputKey.value,
        value : inputValue.value
    }
}

function setContentHtml(input){
    if(typeof input == 'string'){
        content.innerHTML = input;
    }
    else{
        console.warn('Input is not a string, ignoring')
    }
}

function alertError(func){
    if(typeof func == 'function'){
        try{
            func()
        }
        catch(err){
            alert(err)
        }
    }
}

function display(input){
    if(typeof input != 'string'){
        input = JSON.stringify(input, null, 4)
    }
    setContentHtml(input)
}
function dataDisplay(obj){
    if(obj && obj.data){
        display(obj.data)
    }
}

registerButton.onclick = (event) => {
    alertError(()=>{
        api.save(getObjectFromFields())
            .then(dataDisplay)
    })
}

deleteButton.onclick = (event) => {
    alertError(()=>{
        api.del(getObjectFromFields().name)
            .then(dataDisplay)
    })
}

getButton.onclick = (event) => {
    alertError(()=>{
        api.filter(getObjectFromFields().name)
            .then(dataDisplay)
    })
}

listButton.onclick = (event) => {
    alertError(()=>{
        api.listAll()
            .then(dataDisplay)
    })
}


api.listAll().then(dataDisplay)